package com.example.calculate;

import java.util.Stack;

class Zhonghou {
    private Stack theStack;
    private Stack theStack2;
    private String input;
    private String output = "";
    String  A="";


    public Zhonghou(String infix) {
        input = infix;
        int stackSize = input.length();
        theStack = new Stack();
        theStack2 = new Stack();
    }
    public String doTrans() {
        for (int j = 0; j < input.length(); j++) {
            char ch = input.charAt(j);
            switch (ch) {
                case '+':
                case '-':
                    gotOper(ch, 1);
                    break;
                case '*':
                case '/':
                    gotOper(ch, 2);
                    break;
                case '(':
                    theStack.push(ch);
                    break;
                case ')':
                    gotParen(ch);
                    break;
                default:
                    output = output + ch;

                    A+="OPND："+output+" OPTR："+ theStack +'\n';
                    break;
            }
        }
        while (!theStack.isEmpty()) {
            output = output + theStack.pop();
            //        A+="OPND："+output+" OPTR："+theStack.toString()+"\n";
        }
        //System.out.println(output);
        return output;
    }
    public void gotOper(char opThis, int prec1) {
        while (!theStack.isEmpty()) {
            char opTop = (char) theStack.pop();
            if (opTop == '(') {
                theStack.push(opTop);
                break;
            }
            else {
                int prec2;
                if (opTop == '+' || opTop == '-')
                    prec2 = 1;
                else
                    prec2 = 2;
                if (prec2 < prec1) {
                    theStack.push(opTop);
                    break;
                }
                else
                    output = output + opTop;
            }
        }
        theStack.push(opThis);
    }
    public void gotParen(char ch){
        while (!theStack.isEmpty()) {
            char chx = (char) theStack.pop();
            if (chx == '(')
                break;
            else
                output = output + chx;
        }
    }




    public String getA(){
        return A;
    }

}
