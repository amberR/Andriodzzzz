package com.example.calculate;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText editText_random = (EditText)findViewById(R.id.editText);
        final EditText editText_intopostfix = (EditText)findViewById(R.id.editText1);
        final EditText editText_culculate = (EditText)findViewById(R.id.editText2);
        final EditText editText_ai = (EditText)findViewById(R.id.editText3);


        Button button = (Button)findViewById(R.id.button);
        Button button1 = (Button)findViewById(R.id.button1);
        Button button2 = (Button)findViewById(R.id.button2);
        Button button3 = (Button)findViewById(R.id.button3);


        //按下Generate the title，生成题目
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                levelclass lc = new levelclass();
                ArrayList a = lc.produce();
                String b = String.valueOf(a.get(0));
                editText_random.setText(b);

            }

        });

        //按下into    postfix，中缀转后缀
        button1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String infix = editText_random.getText().toString();
                String postfix;
                Zhonghou theTrans = new Zhonghou(infix);
                postfix = theTrans.doTrans();
                editText_intopostfix.setText(postfix);

            }
        });

        //按下culculate，后缀计算
        button2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String suffix;
                suffix = editText_intopostfix.getText().toString();
                String hou = suffix;
                int length = hou.length();
                char[] value = new char[length << 1];
                for (int i=0, j=0; i<length; ++i, j = i << 1) {
                    value[j] = hou.charAt(i);
                    value[1 + j] = ' ';
                }

                String houzhui = new String(value);
                double result;
                Jisuan j = new Jisuan();
                suffix = houzhui;
                result =  j.evaluate(suffix);
                editText_culculate.setText(String.valueOf(result));


            }

        });

        button3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String infix = editText_random.getText().toString();
                String postfix,buzhou;
                Zhonghou theTrans = new Zhonghou(infix);
                postfix = theTrans.doTrans();
                buzhou = theTrans.getA();
                editText_ai.setText(buzhou);

            }
        });

    }
}
