package com.example.thirdactivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;
import android.graphics.Color;
import android.widget.AnalogClock;

public class MainActivity extends Activity implements OnTouchListener {

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button tv = (Button) findViewById(R.id.Buttons);
        tv.setOnTouchListener(this);

        Toast toast = Toast.makeText(this,"20172324",Toast.LENGTH_LONG);
        toast.show();

    }


    @Override
    public boolean onTouch(View arg0, MotionEvent event) {
        Intent intent = new Intent(this, ThirdActivity.class);
        startActivity(intent);
        return true;
    }

}