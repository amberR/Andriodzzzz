package com.example.intopostfix;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText editText_input = (EditText)findViewById(R.id.editText);
        final EditText editText_output = (EditText)findViewById(R.id.editText2);
        Button btnTrans = (Button)findViewById(R.id.button);

        btnTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String infix = editText_input.getText().toString();
                String postfix;

                class Zhonghou {
                    private Stack theStack;
                    private String input;
                    private String output = "";
                    public Zhonghou(String infix) {
                        input = infix;
                        int stackSize = input.length();
                        theStack = new Stack(stackSize);
                    }
                    public String doTrans() {
                        for (int j = 0; j < input.length(); j++) {
                            char ch = input.charAt(j);
                            switch (ch) {
                                case '+':
                                case '-':
                                    gotOper(ch, 1);
                                    break;
                                case '*':
                                case '/':
                                    gotOper(ch, 2);
                                    break;
                                case '(':
                                    theStack.push(ch);
                                    break;
                                case ')':
                                    gotParen(ch);
                                    break;
                                default:
                                    output = output + ch;
                                    break;
                            }
                        }
                        while (!theStack.isEmpty()) {
                            output = output + theStack.pop();
                        }
                        //System.out.println(output);
                        return output;
                    }
                    public void gotOper(char opThis, int prec1) {
                        while (!theStack.isEmpty()) {
                            char opTop = theStack.pop();
                            if (opTop == '(') {
                                theStack.push(opTop);
                                break;
                            }
                            else {
                                int prec2;
                                if (opTop == '+' || opTop == '-')
                                    prec2 = 1;
                                else
                                    prec2 = 2;
                                if (prec2 < prec1) {
                                    theStack.push(opTop);
                                    break;
                                }
                                else
                                    output = output + opTop;
                            }
                        }
                        theStack.push(opThis);
                    }
                    public void gotParen(char ch){
                        while (!theStack.isEmpty()) {
                            char chx = theStack.pop();
                            if (chx == '(')
                                break;
                            else
                                output = output + chx;
                        }
                    }


                    class Stack {
                        private int maxSize;
                        private char[] stackArray;
                        private int top;
                        public Stack(int max) {
                            maxSize = max;
                            stackArray = new char[maxSize];
                            top = -1;
                        }
                        public void push(char j) {
                            stackArray[++top] = j;
                        }
                        public char pop() {
                            return stackArray[top--];
                        }
                        public char peek() {
                            return stackArray[top];
                        }
                        public boolean isEmpty() {
                            return (top == -1);
                        }
                    }
                }
                Zhonghou theTrans = new Zhonghou(infix);
                postfix = theTrans.doTrans();
                editText_output.setText(postfix);
            }
        });

    }
}
