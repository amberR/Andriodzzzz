package com.example.helloworld;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        String TextValue = intent.getStringExtra("Hello World 20172324 23 25");
        System.out.println(TextValue);
        Toast toast = Toast.makeText(this, TextValue,Toast.LENGTH_LONG);
        toast.show();
    }
}
