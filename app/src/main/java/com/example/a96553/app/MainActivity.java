package com.example.a96553.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Stack;

public class MainActivity extends AppCompatActivity {
    Stack<String> stack = new Stack();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        String TextValue = intent.getStringExtra("Hello");
        System.out.println(TextValue);
        Toast toast = Toast.makeText(this, TextValue,Toast.LENGTH_LONG);
        toast.show();

        Button button1 = (Button)findViewById(R.id.button1);
        button1.setOnClickListener(new myButtonOnClickListener1());
        Button button2 = (Button)findViewById(R.id.button2);
        button2.setOnClickListener(new myButtonOnClickListener2());
    }
    public class myButtonOnClickListener1 implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            EditText editText1 = (EditText)findViewById(R.id.editText1);
            stack.push(editText1.getText().toString());
            EditText editText2 = (EditText)findViewById(R.id.editText2);
//            stack.pop(editText2.getText().toString());
            editText2.setMovementMethod(ScrollingMovementMethod.getInstance());
            editText2.setSelection(editText2.getText().length(),editText2.getText().length());
           editText2.setText(editText2.getText().append(stack.toString()+"\n"+"当前操作为：push "+editText1.getText().toString()));
//            "当前栈为："+
        }
    }
    public class myButtonOnClickListener2 implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            EditText editText1 = (EditText)findViewById(R.id.editText1);
            stack.pop();
            EditText editText2 = (EditText)findViewById(R.id.editText2);
//            stack.pop(editText2.getText().toString());
            editText2.setMovementMethod(ScrollingMovementMethod.getInstance());
            editText2.setSelection(editText2.getText().length(),editText2.getText().length());
           editText2.setText(editText2.getText().append(stack.toString()+"\n"+"当前操作为：pop "+editText1.getText().toString()));
        }
 //       "当前栈为："+
    }
}
